# README #

Hello!
This is ezaMaze

Thanks to Brackeys for their bountiful collection of resources.
The Main Menu and Pause Menu were both created using tutorials from Brackeys.

Thanks to Alex Peterson for their space themed skybox generator.
I played around with the app for awhile but ended up using one of the images provided by the program.
http://alexcpeterson.com/spacescape/

Thanks to Jasper Flick of Catlike Coding.
The Unity tutorials on their site helped me when I first started with Unity.
The foundation for this game was created using their Maze generator tutorial.
https://catlikecoding.com/unity/tutorials/maze/

And thank you to my incredibly talented roommate Riley.
The song in the game was made with Riley on clarinet and me on ukelele. 

Thanks for Playing!
