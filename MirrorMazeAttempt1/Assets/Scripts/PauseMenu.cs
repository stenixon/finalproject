﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu;
    public GameObject helpMenu;
    public string mainMenu = "Menu";
    public static bool isPaused;
    public static bool isHelp = true;

    void Update(){
        if(Input.GetKeyDown(KeyCode.Escape)){
            if(isPaused){
                Resume();
            } else {
                Pause();
            }
        }
        if(Input.GetKeyDown(KeyCode.H)){
            helpMenu.SetActive(isHelp);
            isHelp = !isHelp;
        }
        
    }

    public void Resume() {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;

    }
    void Pause() {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }

    public void LoadMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadScene(mainMenu); //fix
    }

    public void QuitGame(){
        Debug.Log("quit");
        Application.Quit();
    }
}
