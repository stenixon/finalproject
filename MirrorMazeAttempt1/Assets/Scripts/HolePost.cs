﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolePost : MonoBehaviour
{
    private GameObject gravityGO;
    private GravityHandler gravityHandler;  
    private MeshRenderer boxColor;
    public void Awake() {
        gravityGO = GameObject.FindGameObjectWithTag("GravityHandler");
        gravityHandler = gravityGO.GetComponent<GravityHandler>();
        boxColor= GetComponent<MeshRenderer>();
        gravityHandler.addHole(boxColor);
    }
public void OnTriggerEnter(Collider other){
        gravityHandler.changeGravity();

}
}
