﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundingBox : MonoBehaviour
{
	private GameObject[,] topBoxes;
	private GameObject[,] botBoxes;
    public GameObject BoundedBox;
    private IntVector2 size;
    public void Generate (IntVector2 size) {
        this.size = size;
		topBoxes = new GameObject[size.x, size.z];
		botBoxes = new GameObject[size.x, size.z];
		for (int x = 0; x < size.x; x++) {
			for (int z = 0; z < size.z; z++) {
				CreateBox(new IntVector2(x, z));
			}
		}
	}

	private void CreateBox (IntVector2 coordinates) {
		GameObject topBox = Instantiate(BoundedBox) as GameObject;
		GameObject botBox = Instantiate(BoundedBox) as GameObject;
		topBoxes[coordinates.x, coordinates.z] = topBox;
		botBoxes[coordinates.x, coordinates.z] = botBox;
		topBox.name = "Top Bounding Box" + coordinates.x + ", " + coordinates.z;
		botBox.name = "Bottom Bounding Box" + coordinates.x + ", " + coordinates.z;
		topBox.transform.parent = transform;
		botBox.transform.parent = transform;
		topBox.transform.localPosition = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, 1f, coordinates.z - size.z * 0.5f + 0.5f);
		botBox.transform.localPosition = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, -1f, coordinates.z - size.z * 0.5f + 0.5f);
	}
}
