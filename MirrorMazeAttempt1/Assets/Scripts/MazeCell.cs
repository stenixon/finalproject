﻿using UnityEngine;

//Create MazeHole class that inherits MazeCell?
public class MazeCell : MonoBehaviour {

	public IntVector2 coordinates;

	private MazeCellEdge[] edges = new MazeCellEdge[MazeDirections.Count];
	private MazeCellEdge[] edgesMirror = new MazeCellEdge[MazeDirections.Count];

	private int initializedEdgeCount;
	private int initializedEdgeCountMirror;

	public bool IsFullyInitialized {
		get {
			return initializedEdgeCount == MazeDirections.Count;
		}
	}	
	public bool IsFullyInitializedMirror {
		get {
			return initializedEdgeCountMirror == MazeDirections.Count;
		}
	}

	public MazeDirection RandomUninitializedDirection {
		get {
			int skips = Random.Range(0, MazeDirections.Count - initializedEdgeCount);
			for (int i = 0; i < MazeDirections.Count; i++) {
				if (edges[i] == null) {
					if (skips == 0) {
						return (MazeDirection)i;
					}
					skips -= 1;
				}
			}
			throw new System.InvalidOperationException("MazeCell has no uninitialized directions left.");
		}
	}

	public MazeDirection RandomUninitializedDirectionMirror {
		get {
			int skips = Random.Range(0, MazeDirections.Count - initializedEdgeCountMirror);
			for (int i = 0; i < MazeDirections.Count; i++) {
				if (edgesMirror[i] == null) {
					if (skips == 0) {
						return (MazeDirection)i;
					}
					skips -= 1;
				}
			}
			throw new System.InvalidOperationException("MazeCell has no uninitialized directions (Mirror) left.");
		}
	}

	public MazeCellEdge GetEdge (MazeDirection direction) {
		return edges[(int)direction];
	}

	public void SetEdge (MazeDirection direction, MazeCellEdge edge) {
		edges[(int)direction] = edge;
		initializedEdgeCount += 1;
	}	
	public MazeCellEdge GetEdgeMirror (MazeDirection direction) {
		return edgesMirror[(int)direction];
	}

	public void SetEdgeMirror (MazeDirection direction, MazeCellEdge edge) {
		edgesMirror[(int)direction] = edge;
		initializedEdgeCountMirror += 1;
	}
}