﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationController : MonoBehaviour
{
    private bool canMove;
    public float rotationAccel = 10f;
    public float maxSpeed = 45f;
    // Start is called before the first frame update
    void Start()
    {
        canMove = false;
    }

    public void enableMove() {
        canMove = true;
    }
    // Update is called once per frame
    void Update()
    {
        if(!canMove) return;
        float smooth = 5.0f;
        float tiltAngle = 60.0f;
        float tiltAroundZ = Input.GetAxis("Horizontal") * tiltAngle * -1f;
        float tiltAroundX = Input.GetAxis("Vertical") * tiltAngle;
        // Rotate the cube by converting the angles into a quaternion.
        Quaternion target = Quaternion.Euler(tiltAroundX, 0, tiltAroundZ);
        
        // Dampen towards the target rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, target,  Time.deltaTime * smooth);
    }
}
