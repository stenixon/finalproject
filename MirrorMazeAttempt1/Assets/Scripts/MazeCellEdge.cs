﻿using UnityEngine;

public abstract class MazeCellEdge : MonoBehaviour {

	public MazeCell cell, otherCell;

	public MazeDirection direction;

	public abstract void Initialize(MazeCell cell, MazeCell otherCell, MazeDirection direction);
}