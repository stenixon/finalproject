﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGoal : MazeCell{
    public void isTopSide(bool topside) {
        if(!topside){
            GameObject[] flippers = GameObject.FindGameObjectsWithTag("GoalFlip");
            GameObject cylinder = GameObject.FindGameObjectWithTag("Goal");
            cylinder.transform.Translate(0f, -1f, 0f);
            foreach(GameObject flip in flippers){
                flip.transform.Rotate(180f,0f,0f);

            }
        }
    }
}

