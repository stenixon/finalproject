﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public Maze mazePrefab;
	public GravityHandler gravhandlerPrefab;
	public IntVector2 size;

	private Maze mazeInstance;
	private GravityHandler gravInstance;

	public GameObject topcam;
	public GameObject botcam;

	private void Start () {
		BeginGame();
	}
	
	private void Update () {
		if (Input.GetKeyDown(KeyCode.Space)) {
			RestartGame();
		}
	}

	private void BeginGame () {
		mazeInstance = Instantiate(mazePrefab) as Maze;
		mazeInstance.size = this.size;
		gravInstance = Instantiate(gravhandlerPrefab) as GravityHandler;
		StartCoroutine(mazeInstance.Generate());
	}

	private void RestartGame () {
		StopAllCoroutines();
		Destroy(mazeInstance.gameObject);
		Destroy(gravInstance.gameObject);
		BeginGame();
	}

	public void RestartBigger () {
		size.x = size.x + 1;
		size.z = size.z + 1;
		topcam.transform.Translate(0f,0f,-1f);
		botcam.transform.Translate(0f,0f,-1f);
		RestartGame();
	}
	
}
