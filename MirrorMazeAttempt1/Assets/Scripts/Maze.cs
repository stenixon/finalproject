﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Maze : MonoBehaviour {
	public GameObject ball;
	private IntVector2 startCoordinates;
	public IntVector2 size;

	public MazeCell cellPrefab;
	public MazeHole holePrefab;
	public MazeGoal goalPrefab;

	public float generationStepDelay;

	public MazePassage passagePrefab;
	public MazeWall wallPrefab;
	public MazeWallMirror wallMirrorPrefab;

	private MazeCell[,] cells;
	private int initializedCells;
	private bool topside;
	private RotationController rotationController;
	private BoundingBox boundingBox;


	public IntVector2 RandomCoordinates {
		get {
			return new IntVector2(Random.Range(0, size.x), Random.Range(0, size.z));
		}
	}

	public bool ContainsCoordinates (IntVector2 coordinate) {
		return coordinate.x >= 0 && coordinate.x < size.x && coordinate.z >= 0 && coordinate.z < size.z;
	}

	public MazeCell GetCell (IntVector2 coordinates) {
		return cells[coordinates.x, coordinates.z];
	}

	public IEnumerator Generate () {
		topside = true;
		initializedCells = 0;
		rotationController = GetComponent<RotationController>();
		boundingBox = GetComponent<BoundingBox>();
		boundingBox.Generate(size);
		WaitForSeconds delay = new WaitForSeconds(generationStepDelay);
		cells = new MazeCell[size.x, size.z];
		List<MazeCell> activeCells = new List<MazeCell>();
		List<MazeCell> activeCellsMirror = new List<MazeCell>();
		DoFirstGenerationStep(activeCells, activeCellsMirror);
		SpawnBall();
		while (activeCells.Count > 0 || activeCellsMirror.Count > 0) {
			yield return delay;
			DoNextGenerationStep(activeCells, activeCellsMirror);
		}
		rotationController.enableMove();
	}

	private void DoFirstGenerationStep (List<MazeCell> activeCells, List<MazeCell> activeCellsMirror) {
		startCoordinates = RandomCoordinates;
		MazeCell firstCell = CreateCellNoHole(startCoordinates);
		activeCells.Add(firstCell);
		activeCellsMirror.Add(firstCell);
		initializedCells++;
	}

	private void DoNextGenerationStep (List<MazeCell> activeCells, List<MazeCell> activeCellsMirror) {
		//TODO: add handler for switching sides/ backtracking
		int currentIndex;
		MazeCell currentCell;
		MazeDirection direction;
		IntVector2 coordinates;
		if(topside){//switch sides if Mirror isFullyInitialized
			currentIndex = activeCells.Count - 1;
			if(currentIndex < 0) {
				topside = false;
				return;
			}
			currentCell = activeCells[currentIndex];
			if (currentCell.IsFullyInitialized) { 
				activeCells.RemoveAt(currentIndex);
				if(currentCell is MazeHole) {
					topside = !topside;
					if(currentCell.IsFullyInitializedMirror) {
						activeCellsMirror.Remove(currentCell);
						return;
					}
				}
				return;
			}
			direction = currentCell.RandomUninitializedDirection;
			coordinates = currentCell.coordinates + direction.ToIntVector2();
			if (ContainsCoordinates(coordinates)) {
				MazeCell neighbor = GetCell(coordinates);
				if (neighbor == null) {
					neighbor = CreateCell(coordinates);
					CreatePassage(currentCell, neighbor, direction);
					activeCells.Add(neighbor);
					activeCellsMirror.Add(neighbor);
			}
			else {
				CreateWall(currentCell, neighbor, direction);
			}
		}
		else {
			CreateWall(currentCell, null, direction);
		}
		}
		currentIndex = activeCellsMirror.Count - 1;
		if(currentIndex < 0) {
			topside = true;
			return;
		}
		currentCell = activeCellsMirror[currentIndex];
			if (currentCell.IsFullyInitializedMirror) { //switch sides if Mirror isFullyInitialized
			activeCellsMirror.RemoveAt(currentIndex);
			if(currentCell is MazeHole) topside = !topside;
			return;
			}
		direction = currentCell.RandomUninitializedDirectionMirror;
		coordinates = currentCell.coordinates + direction.ToIntVector2();
		if (ContainsCoordinates(coordinates)) {
			MazeCell neighbor = GetCell(coordinates);
			if (neighbor == null) {
				neighbor = CreateCell(coordinates);
				CreatePassage(currentCell, neighbor, direction);
				activeCells.Add(neighbor);
				activeCellsMirror.Add(neighbor);
			}
			else {
				CreateWallMirror(currentCell, neighbor, direction);
			}
		}
		else {
			CreateWallMirror(currentCell, null, direction);
		}
	}

	private MazeCell CreateCell (IntVector2 coordinates) { //should pass to CreateHole 10% of the time
	initializedCells++;
	if(initializedCells == size.x * size.z) return CreateGoal(coordinates);
	int makeHole = Random.Range(0, 10);
	if(makeHole == 9) return CreateHole(coordinates);
	else return CreateCellNoHole(coordinates);
	}
	private MazeCell CreateCellNoHole (IntVector2 coordinates) {
		MazeCell newCell = Instantiate(cellPrefab) as MazeCell;
		cells[coordinates.x, coordinates.z] = newCell;
		newCell.coordinates = coordinates;
		newCell.name = "Maze Cell " + coordinates.x + ", " + coordinates.z;
		newCell.transform.parent = transform;
		newCell.transform.localPosition = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, 0f, coordinates.z - size.z * 0.5f + 0.5f);
		return newCell;
	}

	private void CreatePassage (MazeCell cell, MazeCell otherCell, MazeDirection direction) {
		MazePassage passage = Instantiate(passagePrefab) as MazePassage;
		passage.Initialize(cell, otherCell, direction);
		passage = Instantiate(passagePrefab) as MazePassage;
		passage.Initialize(otherCell, cell, direction.GetOpposite());
	}

	private void CreateWall (MazeCell cell, MazeCell otherCell, MazeDirection direction) {
		MazeWall wall = Instantiate(wallPrefab) as MazeWall;
		wall.Initialize(cell, otherCell, direction);
		if (otherCell != null) {
			wall = Instantiate(wallPrefab) as MazeWall;
			wall.Initialize(otherCell, cell, direction.GetOpposite());
		}
	}

	private void CreateWallMirror (MazeCell cell, MazeCell otherCell, MazeDirection direction) {
		MazeWallMirror wall = Instantiate(wallMirrorPrefab) as MazeWallMirror;
		wall.Initialize(cell, otherCell, direction);
		if (otherCell != null) {
			wall = Instantiate(wallMirrorPrefab) as MazeWallMirror;
			wall.Initialize(otherCell, cell, direction.GetOpposite());
		}
	}

	//TODO CreateHole
	private MazeCell CreateHole (IntVector2 coordinates) { //should pass to CreateHole 10% of the time
	topside = !topside; //switch sides
		MazeCell newCell = Instantiate(holePrefab) as MazeHole;
		cells[coordinates.x, coordinates.z] = newCell;
		newCell.coordinates = coordinates;
		newCell.name = "Maze Hole " + coordinates.x + ", " + coordinates.z;
		newCell.transform.parent = transform;
		newCell.transform.localPosition = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, 0f, coordinates.z - size.z * 0.5f + 0.5f);
		return newCell;
	}

	private void SpawnBall() {
		GameObject playerBall = Instantiate(ball) as GameObject;
		playerBall.transform.parent = transform;
		playerBall.transform.localPosition = new Vector3(startCoordinates.x - size.x * 0.5f + 0.5f, 0.2f, startCoordinates.z- size.z * 0.5f + 0.5f);
	}

	private MazeCell CreateGoal(IntVector2 coordinates) {
		MazeGoal newCell = Instantiate(goalPrefab) as MazeGoal;
		newCell.isTopSide(topside);
		cells[coordinates.x, coordinates.z] = newCell;
		newCell.coordinates = coordinates;
		newCell.name = "Maze Goal " + coordinates.x + ", " + coordinates.z;
		newCell.transform.parent = transform;
		newCell.transform.localPosition = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, 0f, coordinates.z - size.z * 0.5f + 0.5f);
		return newCell;
	}
}