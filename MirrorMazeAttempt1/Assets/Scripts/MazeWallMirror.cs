using UnityEngine;

public class MazeWallMirror : MazeCellEdge {
    public override void Initialize (MazeCell cell, MazeCell otherCell, MazeDirection direction) {
		this.cell = cell;
		this.otherCell = otherCell;
		this.direction = direction;
		cell.SetEdgeMirror(direction, this);
		transform.parent = cell.transform;
		transform.localPosition = new Vector3(0,-1,0);
		transform.localRotation = direction.ToRotation();
	}
}