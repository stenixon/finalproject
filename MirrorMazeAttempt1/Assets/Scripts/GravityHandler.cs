﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityHandler : MonoBehaviour
{
    public List<MeshRenderer> holes = new List<MeshRenderer>();
    public bool downGrav = true;
    public Material downwards;
    public Material upwards;
    public void changeGravity() {
        Physics.gravity *= -1;
        downGrav = !downGrav;
        foreach(MeshRenderer mr in holes){
            mr.material = downGrav ? downwards : upwards;
        }
    }

    public void addHole(MeshRenderer meshRenderer) {
        holes.Add(meshRenderer);
    }
}
