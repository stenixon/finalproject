﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RotateCylinder : MonoBehaviour
{
    private Transform CylTransform;
    void Awake(){
        CylTransform = GetComponent<Transform>();
        
    }
    void FixedUpdate(){
        CylTransform.Rotate(1f,0f,0f);
    }

    
    private void OnTriggerEnter(Collider other) {
        GameObject manager = GameObject.FindGameObjectWithTag("GameManager");
        manager.GetComponent<GameManager>().RestartBigger();
    }
}
